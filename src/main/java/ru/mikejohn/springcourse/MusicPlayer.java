package ru.mikejohn.springcourse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MGlushkovskiy on 23.09.2020.
 */
public class MusicPlayer {
    //private Music music = new RockMusic();
    private List<Music> musicList = new ArrayList<>();

    private String name;
    private int volume;

    public MusicPlayer () {}

    //внедряем зависимость через конструктор
    public MusicPlayer (List musicList) {
        this.musicList = musicList;
    }

    public void setMusic(List musicList) {
        this.musicList = musicList;
    }

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public List<Music> getMusicList() {
        return musicList;
    }

    public void setMusicList(List<Music> musicList) {
        this.musicList = musicList;
    }
//    public void playMusic(){
//        System.out.println("Playing: " + music.getSong());
//    }

    public void playMusicList(){

        for (Music music: musicList
             ) {
            System.out.println("Playing: " + music.getSong());
        }
    }
}
