package ru.mikejohn.springcourse;

/**
 * Created by MGlushkovskiy on 23.09.2020.
 */
public interface Music {
    String getSong();
}
