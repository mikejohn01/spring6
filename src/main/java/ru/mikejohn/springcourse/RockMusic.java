package ru.mikejohn.springcourse;

/**
 * Created by MGlushkovskiy on 23.09.2020.
 */
public class RockMusic implements Music{
    @Override
    public String getSong() {
        return "Wind cries Mary";
    }
}
